Scale learner
=============
* Code developed at the University of Massachusetts Amherst; Summer 2018
* Call scale_learner.py with Python 2 with '-f' and a data file

Files
-----
* scale_learner.py is the main function; it reads the data file and calls the learner
* tableau.py provides the Tableau class
* objective.py provides the objective function
* make_data.py writes data files to the data_files/ directory

Data file format
----------------
* Data files largely follow the OTSoft / OTHelp TSV format
* First line: list of morphemes in the data set. The data_files here enumerate prefixes (p0, p1, ...) and stems (s0, s1, ...)
* Second line: list of constraints
* Following lines give input/candidate pairs
+ First line contains the input. Inputs are labeled with the morphemes they contain. Three dollar signs $$$ separate the input from the list and one dollar sign $ separates morphemes from each other 
+ Input is separated from candidate by a tab, probability of that candidate, and then a tab-separated list of constraint violations
+ Inputs are only listed for the first candidate; following candidate lines begin with a tab
* Partial example from TOYcategorical.tsv:

| Labels (not in file) |                   |          |    |     |     |       |     |
|:---------------------|:------------------|:---------|:--:|:---:|:---:|:-----:|:---:|
| *Morphemes*          | p0                | p1	      | p2 | s0	 | s1  | s2    | s3  |
| *Constraints*        |                   |          |    | CCC | Max | Align | SSP |
| *Input 1 - Cand 1*   | ape-taba$$$p0$s0  | apetaba  |	0  | 0	 | 1   | 0     | 0   |
| *Input 1 - Cand 2*   |                   | aptaba   |	1  | 0	 | 1   | 0     | 5   |
| *Input 2 - Cand 1*   | ape-ttaba$$$p0$s1 | apettaba |	1  | 0	 | 0   | 1     | 5   |
| *Input 2 - Cand 2*   |                   | apttaba  |	0  | 1	 | 1   | 0     | 1   |
