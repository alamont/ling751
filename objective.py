# Code written by Andrew Lamont
# June 2018
from scipy.optimize import minimize
from tableau import Tableau
import math

##### Objective function #####
def objective(weights, tableaux, constraints, get_scaled, prior='L1', verbose=False):
	# Get observed/expected probabilities from all tableaux
	observed_expected = []
	for tableau in tableaux:
		observed_expected += tableau.oevaluate(weights, get_scaled)
	loss = 0.0
	for oe in observed_expected:
		# observed probability * log of expected probability
		loss -= oe[0] * math.log(oe[1])

	# prior
	if prior == 'L1':
		sigma = 10.0
		summand = 0.0
		for w in weights:
			summand += w
		summand /= 2 * (sigma ** 2)
		loss += summand
		general_weights = weights[:len(constraints)]
		scaled_weights = weights[len(constraints):]
		total_scales = [0.0 for constraint in constraints]
		for i in range(len(scaled_weights)):
			total_scales[i % len(constraints)] += scaled_weights[i]
		if verbose:
			print 'GENE', general_weights
			print 'SCAL', total_scales
			print 'LOSS', loss
			print 'SUMM', summand
	elif prior == 'L2':
		sigma = 1000.0
		summand = 0.0
		for w in weights:
			summand += (w ** 2)
		summand /= 2 * (sigma ** 2)
		loss += summand
	elif prior == 'mixed':
		sigma = 10.0
		summand = 0.0

		general_weights = weights[:len(constraints)]
		for w in general_weights:
			summand += (w ** 2)

		scaled_weights = weights[len(constraints):]
		total_scales = [0.0 for constraint in constraints]
		for i in range(len(scaled_weights)):
			total_scales[i % len(constraints)] += scaled_weights[i]
		for w in total_scales:
			summand += (w ** 2) #* len(morphemes)

		summand /= (2 * (sigma ** 2))
		loss += summand
		if verbose:
			print 'GENE', general_weights
			print 'SCAL', total_scales
			print 'LOSS', loss
			print 'SUMM', summand
	return loss
