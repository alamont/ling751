# Code written by Andrew Lamont
# June 2018

import math

class Tableau:
	# Initialize with input
	def __init__(self, input):
		self.input = input
		self.candidates = []

	# Add candidates as tuples (cand, percentage optimal, [vio1, vio2, ..., vion])
	def add_candidate(self, candidate):
		self.candidates.append(candidate)

	# Print function
	def pretty_print(self):
		print '/' + self.input + '/'
		for candidate in self.candidates:
			print '\t[' + candidate[0] + ']\t' + str(candidate[1]) + '\t' + '\t'.join([str(vio) for vio in candidate[2]])

	# Return probabilities
	def evaluate(self, weights, get_scaled):
		input_morphemes = self.input.split('$')[-2:]
		scaled_weights = get_scaled(weights, input_morphemes)
		harmonies = [0.0 for candidate in self.candidates]
		for i in range(len(self.candidates)):
			for j in range(len(scaled_weights)):
				harmonies[i] += scaled_weights[j] * self.candidates[i][2][j]
		probabilities = [math.exp(-harmony) for harmony in harmonies]
		z = sum(probabilities)
		return [probability / z for probability in probabilities]

	# Return observed probabilities and expected probabilities
	def oevaluate(self, weights, get_scaled):
		observed = [candidate[1] for candidate in self.candidates]
		return zip(observed, self.evaluate(weights, get_scaled))
