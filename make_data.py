def get_ssp(cand):
	total = 0
	ssp_vios = [	['tv'],
			['sv', 'tr'],
			['nv', 'sr', 'tl'],
			['lv', 'nr', 'sl', 'tn'],
			['rv', 'lr', 'nl', 'sn', 'ts'],
			['vv', 'rr', 'll', 'nn', 'ss', 'tt'],
			['vr', 'rl', 'ln', 'ns', 'st'],
			['vl', 'rn', 'ls', 'nt'],
			['vn', 'rs', 'lt'],
			['vs', 'rt'],
			['vt']	]
	for i in range(len(cand) - 1):
		bigram = cand[i:i+2].replace('p', 't').replace('k', 't').replace('m', 'n').replace('N', 'n')
		total += sum([ssp_vios.index(x) for x in ssp_vios if bigram in x])
	return str(total)

prefixes = ['am', 'an', 'aN']
stems = ['taba', 'ttaba', 'saba', 'tsaba', 'naba', 'tnaba', 'laba', 'tlaba', 'raba', 'traba', 'vaba', 'tvaba', 	'staba', 'ssaba', 'snaba', 'slaba', 'sraba', 'svaba', 'ntaba', 'nsaba', 'nnaba', 'nlaba', 'nraba', 'nvaba', 'ltaba', 'lsaba', 'lnaba', 'llaba', 'lraba', 'lvaba', 'rtaba', 'rsaba', 'rnaba', 'rlaba', 'rraba', 'rvaba', 'vtaba', 'vsaba', 'vnaba', 'vlaba', 'vraba', 'vvaba']

faith_max = '0'
unfaith_max = '1'

#datas = ['CCCcategorical', 'CCCvariation1', 'CCClexical1', 'SSPvariation1', 'SSPlexical1', 'SSPcategorical']
datas = ['SSPvariation1-nasal', 'SSPlexical1-nasal', 'SSPcategorical-nasal']

for data in datas:
	printstring = ''

	for i in range(len(prefixes)):
		printstring += 'p' + str(i) + '\t'
	for i in range(len(stems)):
		printstring += 's' + str(i) + '\t'
	printstring+= '\n'

	printstring += '\t\t\t*CCC\tMax\tAlign\tSSP\n'

	if data == 'CCCcategorical':
		# always delete with C-stems
		# never delete with CC-stems
		for i in range(len(prefixes)):
			prefix = prefixes[i]
			for j in range(len(stems)):
				stem = stems[j]
				input = prefix + 'e-' + stem + '$$$' + 'p' + str(i) + '$' + 's' + str(j)
				faithful_cand = prefix + 'e' + stem
				unfaithful_cand = prefix + stem
				faith_ssp = get_ssp(faithful_cand)
				unfaith_ssp = get_ssp(unfaithful_cand)
				if stem.index('a') == 1:
					# C-stem
					#                                                    PROB  CCC    MAX          ALIGN      SSP
					printstring += input + '\t' + faithful_cand   + '\t' + '0\t0\t' + faith_max   + '\t1\t' + faith_ssp   + '\n'
					printstring +=         '\t' + unfaithful_cand + '\t' + '1\t0\t' + unfaith_max + '\t0\t' + unfaith_ssp + '\n'
				else:
					# CC-stem
					#                                                    PROB  CCC    MAX          ALIGN      SSP
					printstring += input + '\t' + faithful_cand   + '\t' + '1\t0\t' + faith_max   + '\t1\t' + faith_ssp   + '\n'
					printstring +=         '\t' + unfaithful_cand + '\t' + '0\t1\t' + unfaith_max + '\t0\t' + unfaith_ssp + '\n'
	elif data == 'CCCvariation1':
		# always delete with C-stems
		# 0.333 delete with CC-stems
		for i in range(len(prefixes)):
			prefix = prefixes[i]
			for j in range(len(stems)):
				stem = stems[j]
				input = prefix + 'e-' + stem + '$$$' + 'p' + str(i) + '$' + 's' + str(j)
				faithful_cand = prefix + 'e' + stem
				unfaithful_cand = prefix + stem
				faith_ssp = get_ssp(faithful_cand)
				unfaith_ssp = get_ssp(unfaithful_cand)
				if stem.index('a') == 1:
					# C-stem
					#                                                    PROB  CCC    MAX          ALIGN      SSP
					printstring += input + '\t' + faithful_cand   + '\t' + '0\t0\t' + faith_max   + '\t1\t' + faith_ssp   + '\n'
					printstring +=         '\t' + unfaithful_cand + '\t' + '1\t0\t' + unfaith_max + '\t0\t' + unfaith_ssp + '\n'
				else:
					# CC-stem
					#                                                        PROB  CCC    MAX          ALIGN      SSP
					printstring += input + '\t' + faithful_cand   + '\t' + '0.667\t0\t' + faith_max   + '\t1\t' + faith_ssp   + '\n'
					printstring +=         '\t' + unfaithful_cand + '\t' + '0.333\t1\t' + unfaith_max + '\t0\t' + unfaith_ssp + '\n'
	elif data == 'CCClexical1':
		# always delete with C-stems
		# ap- always deletes with CC-stems
		# other prefixes never delete with CC-stems
		for i in range(len(prefixes)):
			prefix = prefixes[i]
			for j in range(len(stems)):
				stem = stems[j]
				input = prefix + 'e-' + stem + '$$$' + 'p' + str(i) + '$' + 's' + str(j)
				faithful_cand = prefix + 'e' + stem
				unfaithful_cand = prefix + stem
				faith_ssp = get_ssp(faithful_cand)
				unfaith_ssp = get_ssp(unfaithful_cand)
				if stem.index('a') == 1:
					# C-stem
					#                                                    PROB  CCC    MAX          ALIGN      SSP
					printstring += input + '\t' + faithful_cand   + '\t' + '0\t0\t' + faith_max   + '\t1\t' + faith_ssp   + '\n'
					printstring +=         '\t' + unfaithful_cand + '\t' + '1\t0\t' + unfaith_max + '\t0\t' + unfaith_ssp + '\n'
				else:
					# CC-stem
					if prefix == 'ap':
						#                                                    PROB  CCC    MAX          ALIGN      SSP
						printstring += input + '\t' + faithful_cand   + '\t' + '0\t0\t' + faith_max   + '\t1\t' + faith_ssp   + '\n'
						printstring +=         '\t' + unfaithful_cand + '\t' + '1\t1\t' + unfaith_max + '\t0\t' + unfaith_ssp + '\n'
					else:
						#                                                    PROB  CCC    MAX          ALIGN      SSP
						printstring += input + '\t' + faithful_cand   + '\t' + '1\t0\t' + faith_max   + '\t1\t' + faith_ssp   + '\n'
						printstring +=         '\t' + unfaithful_cand + '\t' + '0\t1\t' + unfaith_max + '\t0\t' + unfaith_ssp + '\n'
	elif data.startswith('SSPvariation1'):
		# delete 67% of time when SSP > 5
		# else delete 100%
		# CC-stems: delete 10% of time when SSP > 5
		# else delete 25%
		for i in range(len(prefixes)):
			prefix = prefixes[i]
			for j in range(len(stems)):
				stem = stems[j]
				input = prefix + 'e-' + stem + '$$$' + 'p' + str(i) + '$' + 's' + str(j)
				faithful_cand = prefix + 'e' + stem
				unfaithful_cand = prefix + stem
				faith_ssp = get_ssp(faithful_cand)
				unfaith_ssp = get_ssp(unfaithful_cand)
				if stem.index('a') == 1:
					# C-stem
					if int(unfaith_ssp) > 5:
						#                                                    PROB      CCC    MAX          ALIGN      SSP
						printstring += input + '\t' + faithful_cand   + '\t' + '0.333\t0\t' + faith_max   + '\t1\t' + faith_ssp   + '\n'
						printstring +=         '\t' + unfaithful_cand + '\t' + '0.667\t0\t' + unfaith_max + '\t0\t' + unfaith_ssp + '\n'
					else:
						#                                                    PROB  CCC    MAX          ALIGN      SSP
						printstring += input + '\t' + faithful_cand   + '\t' + '0\t0\t' + faith_max   + '\t1\t' + faith_ssp   + '\n'
						printstring +=         '\t' + unfaithful_cand + '\t' + '1\t0\t' + unfaith_max + '\t0\t' + unfaith_ssp + '\n'
				else:
					# CC-stem
					if int(unfaith_ssp) > 5:
						#                                                    PROB    CCC    MAX          ALIGN      SSP
						printstring += input + '\t' + faithful_cand   + '\t' + '0.9\t0\t' + faith_max   + '\t1\t' + faith_ssp   + '\n'
						printstring +=         '\t' + unfaithful_cand + '\t' + '0.1\t1\t' + unfaith_max + '\t0\t' + unfaith_ssp + '\n'
					else:
						#                                                    PROB     CCC    MAX          ALIGN      SSP
						printstring += input + '\t' + faithful_cand   + '\t' + '0.75\t0\t' + faith_max   + '\t1\t' + faith_ssp   + '\n'
						printstring +=         '\t' + unfaithful_cand + '\t' + '0.25\t1\t' + unfaith_max + '\t0\t' + unfaith_ssp + '\n'
	elif data.startswith('SSPlexical1'):
		# always delete with C-stems
		# ap- deletes with CC-stems with SSP < 5, else never
		# other prefixes never delete with CC-stems
		for i in range(len(prefixes)):
			prefix = prefixes[i]
			for j in range(len(stems)):
				stem = stems[j]
				input = prefix + 'e-' + stem + '$$$' + 'p' + str(i) + '$' + 's' + str(j)
				faithful_cand = prefix + 'e' + stem
				unfaithful_cand = prefix + stem
				faith_ssp = get_ssp(faithful_cand)
				unfaith_ssp = get_ssp(unfaithful_cand)
				if stem.index('a') == 1:
					# C-stem
					#                                                    PROB  CCC    MAX          ALIGN      SSP
					printstring += input + '\t' + faithful_cand   + '\t' + '0\t0\t' + faith_max   + '\t1\t' + faith_ssp   + '\n'
					printstring +=         '\t' + unfaithful_cand + '\t' + '1\t0\t' + unfaith_max + '\t0\t' + unfaith_ssp + '\n'
				else:
					# CC-stem
					if prefix == 'ap':
						if int(unfaith_ssp) > 5:
							#                                                    PROB  CCC    MAX          ALIGN      SSP
							printstring += input + '\t' + faithful_cand   + '\t' + '1\t0\t' + faith_max   + '\t1\t' + faith_ssp   + '\n'
							printstring +=         '\t' + unfaithful_cand + '\t' + '0\t1\t' + unfaith_max + '\t0\t' + unfaith_ssp + '\n'
						else:
							#                                                    PROB  CCC    MAX          ALIGN      SSP
							printstring += input + '\t' + faithful_cand   + '\t' + '0\t0\t' + faith_max   + '\t1\t' + faith_ssp   + '\n'
							printstring +=         '\t' + unfaithful_cand + '\t' + '1\t1\t' + unfaith_max + '\t0\t' + unfaith_ssp + '\n'
					else:
						#                                                    PROB  CCC    MAX          ALIGN      SSP
						printstring += input + '\t' + faithful_cand   + '\t' + '1\t0\t' + faith_max   + '\t1\t' + faith_ssp   + '\n'
						printstring +=         '\t' + unfaithful_cand + '\t' + '0\t1\t' + unfaith_max + '\t0\t' + unfaith_ssp + '\n'
	elif data.startswith('SSPcategorical'):
		# don't delete if SSP > 8
		for i in range(len(prefixes)):
			prefix = prefixes[i]
			for j in range(len(stems)):
				stem = stems[j]
				input = prefix + 'e-' + stem + '$$$' + 'p' + str(i) + '$' + 's' + str(j)
				faithful_cand = prefix + 'e' + stem
				unfaithful_cand = prefix + stem
				faith_ssp = get_ssp(faithful_cand)
				unfaith_ssp = get_ssp(unfaithful_cand)
				faith_ccc = '0'
				if stem.index('a') == 1:
					# C-stem
					unfaith_ccc = '0'
				else:
					# CC-stem
					unfaith_ccc = '1'
				if int(unfaith_ssp) > 8:
					#                                                    PROB      CCC                  MAX          ALIGN      SSP
					printstring += input + '\t' + faithful_cand   + '\t' + '1\t' + faith_ccc   + '\t' + faith_max   + '\t1\t' + faith_ssp   + '\n'
					printstring +=         '\t' + unfaithful_cand + '\t' + '0\t' + unfaith_ccc + '\t' + unfaith_max + '\t0\t' + unfaith_ssp + '\n'
				else:
					#                                                    PROB      CCC                  MAX          ALIGN      SSP
					printstring += input + '\t' + faithful_cand   + '\t' + '0\t' + faith_ccc   + '\t' + faith_max   + '\t1\t' + faith_ssp   + '\n'
					printstring +=         '\t' + unfaithful_cand + '\t' + '1\t' + unfaith_ccc + '\t' + unfaith_max + '\t0\t' + unfaith_ssp + '\n'

	outfile = open('data_files/' + data + '.tsv', 'w')
	outfile.write(printstring)
	outfile.close()
