# Code written by Andrew Lamont
# May 2018
import sys, math, random
from scipy.optimize import minimize
from tableau import Tableau
from objective import objective

# Get scaled weights
def get_scaled(weights, morphemes):
        scale = [0.0 for constraint in constraints]
        for i in range(len(constraints)):
                scale[i] += weights[i]
        for morpheme in morphemes:
                morph_num = int(morpheme[1:])
                index = len(constraints) * (morph_num + 1)
                if morpheme[0] == 's':
                        index += len(constraint) * (number_prefix + 1)
                for i in range(len(constraints)):
                        scale[i] += weights[index + i]
        return scale

##### Read input file #####
input_file = open(sys.argv[sys.argv.index('-f') + 1])

# Get morphemes
morphemes = input_file.readline().split()
number_prefix = 0
for morpheme in morphemes:
	if morpheme[0] == 'p':
		number_prefix = int(morpheme[1:]) + 1
	else:
		break

# Get constraints
constraints = input_file.readline().split()

# Initialize constraint weights
#con_weights = [10.0, 5.0, 10.0, 15.0, #general weights
#		100.0, 0.0, 0.0, 0.0, #p0
#		0.0, 100.0, 0.0, 0.0, #p1
#		0.0, 0.0, 100.0, 0.0, #p2
#		0.0, 0.0, 0.0, 100.0, #s0
#		0.0, 0.0, 0.0, 0.0, #s1
#		0.0, 0.0, 0.0, 0.0, #s2
#		0.0, 0.0, 0.0, 0.0] #s3

con_weights = [0.0 for constraint in constraints]
for morpheme in morphemes:
	con_weights += [0.0 for constraint in constraints]
#con_weights = [random.uniform(0, 10) for constraint in constraints]
#for morpheme in morphemes:
#	con_weights += [random.uniform(0, 10) for constraint in constraints]

# Get tableaux
tableaux = []
this_input = ''
for line in input_file:
	this_line = line.rstrip().split()
	if len(this_line) == 3 + len(constraints):
		this_input = this_line[0]
		tableaux.append(Tableau(this_input))
		this_candidate = (this_line[1], float(this_line[2]), [int(vio) for vio in this_line[3:]])
	else:
		this_candidate = (this_line[0], float(this_line[1]), [int(vio) for vio in this_line[2:]])
	tableaux[-1].add_candidate(this_candidate)

##### Sanity #####
#for tableau in tableaux:
#	tableau.pretty_print()
#	print tableau.oevaluate(con_weights)

##### Blackbox learner to minimize objective function
print
print
print con_weights
print objective(con_weights, tableaux, constraints, get_scaled, verbose=True)

new_weights = minimize(objective, con_weights, args=(tableaux, constraints, get_scaled), method='L-BFGS-B', bounds=[(0, None) for x in con_weights])['x']

print
print
print new_weights
print objective(new_weights, tableaux, constraints, get_scaled, verbose=True)

#for tableau in tableaux:
#	print get_scaled(new_weights, tableau.input.split('$')[-2:])
#	tableau.pretty_print()
#	print tableau.oevaluate(new_weights)

